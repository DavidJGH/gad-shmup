﻿using System;
using Sirenix.Serialization;

public interface IScoreManager
{
    int Score { get; set; }
    Action<float> OnScoreUpdated { get; set; }
}