﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ResourceManager))]
public class CutBoost : MonoBehaviour
{
    private ResourceManager _resourceManager;
    private MovementController _movementController;
    private UIManager _uiManager;

    [SerializeField]
    private float cutValue;

    public Action onCutBoost;

    private void Awake()
    {
        _resourceManager = GetComponent<ResourceManager>();
        _movementController = GetComponent<MovementController>();
    }

    void Start()
    {
        _resourceManager.onDamaged += i =>
        {
            if ((float)_resourceManager.Health / _resourceManager.spaceshipStats.maxHealth <= cutValue)
            {
                Execute();
            }
        };
    }

    void Execute()
    {
        onCutBoost.Invoke();
        _movementController.AllowBoosting = false;
    }
}
