﻿using System;
using DependencyInjection;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace Enemy
{
    [RequireComponent(typeof(ResourceManager))]
    public class EnemyScoreAdder : SerializedMonoBehaviour
    {
        [HideInInspector]
        public SpaceshipStats spaceshipStats;
        public ScoreManager ScoreManager { get; set; }

        private ResourceManager _resourceManager;

        private void Start()
        {
            _resourceManager = GetComponent<ResourceManager>();
            _resourceManager.onDamaged += DamageAddScore;
            _resourceManager.onNoHealth += KillAddScore;
        }

        private void KillAddScore(GameObject obj)
        {
            ScoreManager.Score += spaceshipStats.killPoints;
        }

        private void DamageAddScore(int f)
        {
            ScoreManager.Score += f * spaceshipStats.damagePointsCoefficientPoints;
        }
    }
}