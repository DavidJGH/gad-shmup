﻿using System.Collections;
using System.Collections.Generic;
using DependencyInjection;
using Input;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace Enemy
{
    public class EnemySpawningManager : SerializedMonoBehaviour
    {
        [ResolveDependency]
        public IScoreManager ScoreManager { get; set; }
        
        [ResolveDependency]
        public IPlayerDependency PlayerDependency { get; set; }
        
        private Transform _player;

        private int _index;
    
        private List<GameObject> _spaceshipEnemies = new List<GameObject>();
        [SerializeField]
        private GameObject spaceshipEnemy;

        [SerializeField]
        private float spaceshipPoolSize;

        public float SpaceshipPoolSize {
            get => spaceshipPoolSize;
            set
            {
                if (Mathf.Floor(value) > spaceshipPoolSize)
                {
                    while (_spaceshipEnemies.Count < value)
                    {
                        GameObject instance = Instantiate(spaceshipEnemy);
                        instance.GetComponent<EnemyInputManager>().player = _player;
                        instance.GetComponent<EnemyScoreAdder>().ScoreManager = (ScoreManager) ScoreManager;
                        SetTransforms(instance.transform);
                        if (instance.TryGetComponent(out ResourceManager resourceManager))
                        {
                            resourceManager.onNoHealth += ResetEnemy;
                        }

                        _spaceshipEnemies.Add(instance);
                    }
                }
                
                spaceshipPoolSize = value;
            }
        }
        

        private List<GameObject> _orbEnemies = new List<GameObject>();
        [SerializeField]
        private GameObject orbEnemy;

        [SerializeField]
        private float orbPoolSize;

        public float OrbPoolSize {
            get => orbPoolSize;
            set
            {
                if (Mathf.Floor(value) > orbPoolSize)
                {
                    while (_orbEnemies.Count < value)
                    {
                        GameObject instance = Instantiate(orbEnemy);
                        instance.GetComponent<EnemyInputManager>().player = _player;
                        instance.GetComponent<EnemyScoreAdder>().ScoreManager = (ScoreManager) ScoreManager;
                        SetTransforms(instance.transform);
                        if (instance.TryGetComponent(out ResourceManager resourceManager))
                        {
                            resourceManager.onNoHealth += ResetEnemy;
                        }
                        _orbEnemies.Add(instance);
                    }
                }
                
                orbPoolSize = value;
            }
        }
    
        [OdinSerialize]
        public float MinPlayerSpawnDistance { get; set; }
        [OdinSerialize]
        public float SpawnRange { get; set; }

        [SerializeField]
        private float poolIncreasePerKill = .34f;
    
        void Start()
        {
            _player = ((PlayerDependency)PlayerDependency).transform;

            while (_spaceshipEnemies.Count < spaceshipPoolSize)
            {
                GameObject instance = Instantiate(spaceshipEnemy);
                instance.GetComponent<EnemyInputManager>().player = _player;
                instance.GetComponent<EnemyScoreAdder>().ScoreManager = (ScoreManager) ScoreManager;
                SetTransforms(instance.transform);
                if (instance.TryGetComponent(out ResourceManager resourceManager))
                {
                    resourceManager.onNoHealth += ResetEnemy;
                }

                _spaceshipEnemies.Add(instance);
            }
            while (_orbEnemies.Count < orbPoolSize)
            {
                GameObject instance = Instantiate(orbEnemy);
                instance.GetComponent<EnemyInputManager>().player = _player;
                instance.GetComponent<EnemyScoreAdder>().ScoreManager = (ScoreManager) ScoreManager;
                SetTransforms(instance.transform);
                if (instance.TryGetComponent(out ResourceManager resourceManager))
                {
                    resourceManager.onNoHealth += ResetEnemy;
                }
                _orbEnemies.Add(instance);
            }
        }

        public void ResetEnemy(GameObject gameObj)
        {
            SpaceshipPoolSize += poolIncreasePerKill;
            if (gameObj.TryGetComponent(out ResourceManager resourceManager))
            {
                resourceManager.ResetHealth();
            }

            StartCoroutine(ResetTransform(gameObj));
        }

        private IEnumerator ResetTransform(GameObject gameObj)
        {
            yield return null;
            SetTransforms(gameObj.transform);
        }

        public void SetTransforms(Transform trans)
        {
            trans.rotation = Random.rotation;
            Vector3 position;
            do
            {
                position = new Vector3(Random.Range(-SpawnRange, SpawnRange), Random.Range(-SpawnRange, SpawnRange),
                    Random.Range(-SpawnRange, SpawnRange));
            } while ((position - _player.position).magnitude > MinPlayerSpawnDistance);
            trans.position = position;
            Rigidbody body = trans.GetComponent<Rigidbody>();
            body.velocity = Vector3.zero;
            body.angularVelocity = Vector3.zero;
        }
    }
}
