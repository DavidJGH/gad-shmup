﻿using System;
using System.Collections;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

namespace Input
{
    public class EnemyInputManager : SerializedMonoBehaviour, IInputManager
    {
        [HideInInspector]
        public SpaceshipStats spaceshipStats;
        
        [HideInInspector]
        public Transform player;

        public Vector2 MovementInput { get; set; }
        public Vector2 RotationInput { get; set; }
        public Action OnStartFire { get; set; }
        public Action OnStopFire { get; set; }
        public Action OnStartBoosting { get; set; }
        public Action OnStopBoosting { get; set; }
    
        void Start()
        {
            OnStartFire?.Invoke();
        }

        void Update()
        {
            Vector3 horAxis = transform.right;
            Vector3 verAxis = transform.up;
            Vector3 toPlayer = player.position - transform.position;
            float dotHor = Vector3.Dot(toPlayer, horAxis);
            float dotVer = Vector3.Dot(toPlayer, verAxis);
            RotationInput = new Vector2(dotHor, dotVer);
            MovementInput = new Vector2(0, Mathf.Lerp(-.3f, 1, Mathf.InverseLerp(spaceshipStats.stopStoppingDistance, spaceshipStats.startStoppingDistance, toPlayer.magnitude)));
        }
    }
}
