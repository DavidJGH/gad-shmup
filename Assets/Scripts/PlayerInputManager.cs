﻿using System;
using System.Runtime.CompilerServices;
using DependencyInjection;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Input
{
        public class PlayerInputManager : MonoBehaviour, IInputManager
        {
                [ResolveDependency]
                public IPlayerInputDependency PlayerInputDependency { get; set; }
                private PlayerInput _playerInput;

                public Vector2 MovementInput { get; set; }
                public Vector2 RotationInput { get; set; }
                public Action OnStartFire { get; set; }
                public Action OnStopFire { get; set; }
                public Action OnStartBoosting { get; set; }
                public Action OnStopBoosting { get; set; }

                private void Start()
                {
                        _playerInput = PlayerInputDependency.PlayerInput;
                        _playerInput.currentActionMap["Fire"].started += (context) => OnStartFire?.Invoke();
                        _playerInput.currentActionMap["Fire"].canceled += (context) => OnStopFire?.Invoke();
                        _playerInput.currentActionMap["Boost"].started += (context) => OnStartBoosting?.Invoke();
                        _playerInput.currentActionMap["Boost"].canceled += (context) => OnStopBoosting?.Invoke();
                }

                private void Update()
                {
                        MovementInput = _playerInput.currentActionMap["Move"].ReadValue<Vector2>();
                        RotationInput = _playerInput.currentActionMap["Look"].ReadValue<Vector2>();
                }
        }
}