﻿using UnityEngine.InputSystem;

namespace Input
{
    public interface IPlayerInputDependency
    {
        PlayerInput PlayerInput { get; set; }
    }
}