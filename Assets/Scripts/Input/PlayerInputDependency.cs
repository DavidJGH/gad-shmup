﻿using UnityEngine;
using UnityEngine.InputSystem;

namespace Input
{
    public class PlayerInputDependency : MonoBehaviour, IPlayerInputDependency
    {
        public PlayerInput PlayerInput { get; set; }

        private void Awake()
        {
            PlayerInput = GetComponent<PlayerInput>();
        }
    }
}