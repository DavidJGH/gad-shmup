﻿using System;
using UnityEngine;

namespace Input
{
    public interface IInputManager
    {
        Vector2 MovementInput { get; set; }
        Vector2 RotationInput { get; set; }
        Action OnStartFire { get; set; }
        Action OnStopFire { get; set; }
        Action OnStartBoosting { get; set; }
        Action OnStopBoosting { get; set; }
    }
}