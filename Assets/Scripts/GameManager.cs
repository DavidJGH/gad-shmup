﻿using System;
using System.Collections;
using System.Collections.Generic;
using DependencyInjection;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [ResolveDependency]
    public IPlayerDependency PlayerDependency { get; set; }
    
    [ResolveDependency]
    public IScoreManager ScoreManager { get; set; }

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        ResourceManager resourceManager = ((PlayerDependency) PlayerDependency).GetComponent<ResourceManager>();
        resourceManager.onNoHealth += GameOver;
    }

    void GameOver(GameObject gameObj)
    {
        StartCoroutine(GameOverCoroutine(gameObj));
    }

    private IEnumerator GameOverCoroutine(GameObject gameObj)
    {
        gameObj.transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
        yield return new WaitForSeconds(1);
        String playerName = PlayerPrefs.GetString("name", "player");
        int score = ScoreManager.Score;
        SaveSystem.SaveScore(playerName, score);
        PlayerPrefs.DeleteKey("name");
        SceneManager.LoadScene(2);
    }
}
