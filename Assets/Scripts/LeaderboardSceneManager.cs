﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LeaderboardSceneManager : MonoBehaviour
{
    [SerializeField]
    private Button button;
    [SerializeField]
    private TextMeshProUGUI leaderboardText;
    
    void Start()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        button.onClick.AddListener(BackButton);
        Leaderboard leaderboard = SaveSystem.LoadLeaderboard();
        if (leaderboard is null)
        {
            leaderboardText.text = "no scores yet";
        }
        else
        {
            List<Score> scores = new List<Score>();
            for (int i = 0; i < leaderboard.scores.Length; i++)
            {
                scores.Add(new Score(leaderboard.names[i], leaderboard.scores[i]));
            }

            scores = scores.OrderByDescending(e => e.score).ToList();

            String text = "";
            for (int i = 0; i < scores.Count; i++)
            {
                text += i+1 + ") " + scores[i].score + " // " + scores[i].name + "\n";
            }
            leaderboardText.text = text;
        }
    }

    void BackButton()
    {
        SceneManager.LoadScene(0);
    }

    private class Score
    {
        public string name;
        public int score;

        public Score(string name, int score)
        {
            this.name = name;
            this.score = score;
        }
    }
}
