﻿using UnityEngine.Events;

namespace StateMachine
{
    public class State
    {
        public readonly UnityEvent stateEnter = new UnityEvent();
        public readonly UnityEvent stateExit = new UnityEvent();
    }
}