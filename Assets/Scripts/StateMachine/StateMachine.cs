﻿using UnityEngine;

namespace StateMachine
{
    public class StateMachine : MonoBehaviour
    {
        private State _currentState;
        
        public State CurrentState
        {
            get => _currentState;
            set
            {
                _currentState?.stateExit.Invoke();
                _currentState = value;
                _currentState?.stateEnter.Invoke();
            }
        }
    }
}