﻿using UnityEngine;

namespace StateMachine
{
    public class MovementStates : MonoBehaviour
    {
        public State Moving { get; set; } = new State();
        public State Boosting { get; set; } = new State();
    }
}
