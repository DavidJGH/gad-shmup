﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using DependencyInjection;
using Input;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.InputSystem;

public class ShootManager : SerializedMonoBehaviour
{
    [HideInInspector]
    public SpaceshipStats spaceshipStats;

    [ResolveDependency]
    public IInputManager InputManager { get; set; }

    [SerializeField]
    private Transform[] spawnPositions = new Transform[2];

    private float _cooldownTimer;
    
    private Rigidbody _rb;

    private int _index;
    
    private Queue<GameObject> _projectiles = new Queue<GameObject>();
    [SerializeField]
    private GameObject projectile;


    private bool _shooting;

    void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        InputManager = GetComponent<IInputManager>();
        InputManager.OnStartFire += StartShoot;
        InputManager.OnStopFire += StopShoot;
    }

    private void StopShoot()
    {
        _shooting = false;
    }
    
    private void StartShoot()
    {
        _shooting = true;
    }

    private void Update()
    {
        if (_shooting && Time.time > _cooldownTimer)
        {
            _cooldownTimer = Time.time + spaceshipStats.bulletCooldown;
            Shoot();
            if(spaceshipStats.bothBullets)
                Shoot();
        }
    }

    private void Shoot()
    {
        _shooting = true;
        GameObject proj = null;
        if (_projectiles.Count < spaceshipStats.bulletPoolSize)
            proj = Instantiate(projectile);
        else
            proj = _projectiles.Dequeue();

        proj.GetComponent<MeshRenderer>().enabled = true;
        proj.GetComponent<ParticleSystem>().Stop();
        Rigidbody rbp = proj.GetComponent<Rigidbody>();
        rbp.position = spawnPositions[_index].position;
        rbp.rotation = _rb.rotation;
        rbp.angularVelocity = new Vector3();
        rbp.velocity = _rb.velocity + transform.forward * spaceshipStats.bulletSpeed;
        _projectiles.Enqueue(proj);
        if (_index > spawnPositions.Length - 2)
            _index = 0;
        else
            _index++;
    }
}
