﻿using System;
using System.Collections;
using System.Collections.Generic;
using DependencyInjection;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using StateMachine;
using UnityEngine;

public class VisualsController : SerializedMonoBehaviour
{
    [SerializeField]
    private List<TrailRenderer> boostTrails;

    [SerializeField]
    private ParticleSystem deathParticles;

    private Transform _mesh;

    private Rigidbody _rb;

    private MovementStates _movementStates;

    private Quaternion _startOrientation;

    private ResourceManager _resourceManager;
    
    [OdinSerialize]
    public float RotationDampingCoefficient { get; set; }
    
    void Start()
    {
        _resourceManager = GetComponent<ResourceManager>();
        _movementStates = GetComponent<MovementStates>();
        _movementStates.Boosting.stateEnter.AddListener(Boosting);
        _movementStates.Boosting.stateExit.AddListener(StopBoosting);
        _rb = GetComponent<Rigidbody>();
        _mesh = transform.GetChild(0);
        _startOrientation = _mesh.localRotation;
        
        _resourceManager.onNoHealth += delegate(GameObject o)
        {
            deathParticles.Stop();
            deathParticles.Play();
        };
    }

    private void Update()
    {
        Vector3 localAngularVelocity = transform.InverseTransformDirection(_rb.angularVelocity).normalized
                                       * _rb.angularVelocity.magnitude;
        _mesh.localRotation = Quaternion.Lerp(_mesh.localRotation, _startOrientation * Quaternion.Euler(new Vector3(-localAngularVelocity.y*10 + localAngularVelocity.z*15,localAngularVelocity.x*5,0)), RotationDampingCoefficient);
    }

    private void StopBoosting()
    {
        boostTrails.ForEach(e => e.emitting = false);
    }

    private void Boosting()
    {
        boostTrails.ForEach(e => e.emitting = true);
    }
}
