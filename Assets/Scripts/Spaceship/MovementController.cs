﻿using System;
using DependencyInjection;
using Input;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using StateMachine;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(IInputManager),typeof(StateMachine.StateMachine),typeof(MovementStates))]
public class MovementController : SerializedMonoBehaviour
{
    [HideInInspector]
    public SpaceshipStats spaceshipStats;

    private IInputManager _inputManager;

    private StateMachine.StateMachine _stateMachine;

    private MovementStates _movementStates;

    private Rigidbody _rb;

    private float _mouseLerpedXInput;
    private float _mouseLerpedYInput;
    private float _lerpedRollInput;
    
    private float _maxCurrentVelocity;

    [SerializeField]
    private bool allowBoosting;

    public bool AllowBoosting
    {
        get => allowBoosting;
        set
        {
            allowBoosting = value;
            if (!allowBoosting && _stateMachine.CurrentState == _movementStates.Boosting)
            {
                _stateMachine.CurrentState = _movementStates.Moving;
            }
        }
    }

    void Start()
    {
        _inputManager = GetComponent<IInputManager>();
        _stateMachine = GetComponent<StateMachine.StateMachine>();
        _movementStates = GetComponent<MovementStates>();
        _rb = GetComponent<Rigidbody>();
        _maxCurrentVelocity = spaceshipStats.maxVelocity;
        _inputManager.OnStartBoosting += () =>
        {
            if (_inputManager.MovementInput.y > 0 && allowBoosting)
            {
                _stateMachine.CurrentState = _movementStates.Boosting;
            }
        };
        _inputManager.OnStopBoosting += () =>
        {
            if (_stateMachine.CurrentState == _movementStates.Boosting && allowBoosting)
            {
                _stateMachine.CurrentState = _movementStates.Moving;
            }
        };
        _movementStates.Boosting.stateEnter.AddListener(Boosting);
        _movementStates.Boosting.stateExit.AddListener(StopBoosting);
    }

    private void FixedUpdate()
    {
        if (_inputManager.MovementInput.y <= 0 && _stateMachine.CurrentState == _movementStates.Boosting)
        {
            _stateMachine.CurrentState = _movementStates.Moving;
        }
        Vector3 targetVelocity = transform.rotation * (_maxCurrentVelocity * new Vector3(0, 0, _inputManager.MovementInput.y));
        var velocity = _rb.velocity;
        _rb.velocity = Vector3.Lerp(velocity, targetVelocity, spaceshipStats.dampingCurve.Evaluate(targetVelocity.magnitude < Double.Epsilon ? 0 : velocity.magnitude/targetVelocity.magnitude));
        
        _mouseLerpedXInput = Mathf.Lerp(_mouseLerpedXInput, _inputManager.RotationInput.x, spaceshipStats.inputDampingCoefficient);
        _mouseLerpedYInput = Mathf.Lerp(_mouseLerpedYInput, _inputManager.RotationInput.y, spaceshipStats.inputDampingCoefficient);
        _lerpedRollInput = Mathf.Lerp(_lerpedRollInput, _inputManager.MovementInput.x, spaceshipStats.inputDampingCoefficient);

        float pitch = Mathf.Lerp(0, -_mouseLerpedYInput*spaceshipStats.maxAngularVelocity, spaceshipStats.rotationDampingCoefficient);
        float yaw = Mathf.Lerp(0, _mouseLerpedXInput*spaceshipStats.maxAngularVelocity, spaceshipStats.rotationDampingCoefficient);
        float roll = Mathf.Lerp(0, -_lerpedRollInput*spaceshipStats.maxRollVelocity, spaceshipStats.rotationDampingCoefficient);
        _rb.AddRelativeTorque(new Vector3(pitch, yaw, roll));
    }


    public MovementController(MovementStates movementStates)
    {
        _movementStates = movementStates;
    }

    private void StopBoosting()
    {
        _maxCurrentVelocity = spaceshipStats.maxVelocity;
    }

    private void Boosting()
    {
        _maxCurrentVelocity = spaceshipStats.maxBoostVelocity;
    }
}