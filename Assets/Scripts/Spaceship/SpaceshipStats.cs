﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SpaceshipStats")]
public class SpaceshipStats : ScriptableObject
{
    public int killPoints;
    public int damagePointsCoefficientPoints;
    
    public int maxHealth;
    
    public float startStoppingDistance;
    public float stopStoppingDistance;
    
    public float maxVelocity;
    public float maxBoostVelocity;
    public AnimationCurve dampingCurve;
    public float maxAngularVelocity;
    public float rotationDampingCoefficient;
    public float inputDampingCoefficient;
    public float maxRollVelocity;
    
    public float bulletSpeed;
    public float bulletCooldown;
    public bool bothBullets;
    public int bulletPoolSize;
}
