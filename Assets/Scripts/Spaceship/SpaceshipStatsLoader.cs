﻿using System.Collections;
using System.Collections.Generic;
using Enemy;
using Input;
using UnityEngine;

public class SpaceshipStatsLoader : MonoBehaviour
{
    [SerializeField]
    private SpaceshipStats spaceshipStats;

    private EnemyScoreAdder enemyScoreAdder;
    private ResourceManager resourceManager;
    private EnemyInputManager enemyInputManager;
    private MovementController movementController;
    private ShootManager shootManager;

    private void Awake()
    {
        shootManager = GetComponent<ShootManager>();
        movementController = GetComponent<MovementController>();
        enemyInputManager = GetComponent<EnemyInputManager>();
        resourceManager = GetComponent<ResourceManager>();
        enemyScoreAdder = GetComponent<EnemyScoreAdder>();
        if (shootManager != null) shootManager.spaceshipStats = spaceshipStats;
        if (movementController != null) movementController.spaceshipStats = spaceshipStats;
        if (enemyInputManager != null) enemyInputManager.spaceshipStats = spaceshipStats;
        if (resourceManager != null) resourceManager.spaceshipStats = spaceshipStats;
        if (enemyScoreAdder != null) enemyScoreAdder.spaceshipStats = spaceshipStats;
    }
}
