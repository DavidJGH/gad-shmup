﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Serializable]
public class Leaderboard
{
    public String[] names;
    public int[] scores;

    public Leaderboard(String name, int score, Leaderboard leaderboard = null)
    {
        if (leaderboard is null)
        {
            names = new[] {name};
            scores = new[] {score};
        }
        else
        {
            names = leaderboard.names.Concat(new[] {name}).ToArray();
            scores = leaderboard.scores.Concat(new[] {score}).ToArray();
        }
    }
}
