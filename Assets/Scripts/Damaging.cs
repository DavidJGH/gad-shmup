﻿using Sirenix.OdinInspector;
using Sirenix.Serialization;

public abstract class Damaging : SerializedMonoBehaviour
{
    [OdinSerialize]
    public int DamageValue { get; set; }
}