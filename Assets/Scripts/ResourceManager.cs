﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;
using UnityEngine.Events;

public class ResourceManager : SerializedMonoBehaviour
{
    [HideInInspector]
    public SpaceshipStats spaceshipStats;

    public int Health { get; set; }

    [HideInInspector]
    public Action<GameObject> onNoHealth;
    [HideInInspector]
    public Action<int> onDamaged;

    private bool _noHealthBool;
    
    void Start()
    {
        ResetHealth();
    }

    public void ResetHealth()
    {
        _noHealthBool = false;
        Health = spaceshipStats.maxHealth;
    }
    
    private void OnCollisionEnter(Collision other)
    {
        if(other.transform.TryGetComponent(out Damaging damaging))
        {
            Health -= damaging.DamageValue;
            onDamaged?.Invoke(damaging.DamageValue);
            if (Health <= 0 && !_noHealthBool)
            {
                _noHealthBool = true;
                onNoHealth?.Invoke(gameObject);
            }
        }
    }
}
