﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public static class SaveSystem
{
    private static string _file = "/leaderboard.shmup";
    
    public static void SaveScore(String name, int score)
    {
        string path = Application.persistentDataPath + _file;

        Leaderboard previous = LoadLeaderboard();
        
        BinaryFormatter formatter = new BinaryFormatter();
        FileStream stream = new FileStream(path, FileMode.Create);

        Leaderboard leaderboard = new Leaderboard(name, score, previous);
        formatter.Serialize(stream, leaderboard);
        stream.Close();
    }
    
    public static Leaderboard LoadLeaderboard()
    {
        string path = Application.persistentDataPath + _file;
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            Leaderboard leaderboard = formatter.Deserialize(stream) as Leaderboard;
            stream.Close();
            return leaderboard;
        }
        return null;
    }
}