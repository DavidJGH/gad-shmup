﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class NameManager : MonoBehaviour
{
    [SerializeField]
    private Button button;
    [SerializeField]
    private TMP_InputField inputField;
    
    void Awake()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
        button.onClick.AddListener(DoneButton);
    }

    void DoneButton()
    {
        PlayerPrefs.DeleteKey("name");
        String playerName = inputField.text;
        if (!string.IsNullOrEmpty(playerName))
        {
            PlayerPrefs.SetString("name", playerName);
        }
        SceneManager.LoadScene(1);
    }
}
