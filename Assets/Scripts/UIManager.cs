﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using DependencyInjection;
using Input;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Image = UnityEngine.UIElements.Image;

public class UIManager : MonoBehaviour
{
    [ResolveDependency]
    public IScoreManager ScoreManager { get; set; }
    [ResolveDependency]
    public IPlayerDependency PlayerDependency { get; set; }

    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private Slider healthSlider;
    [SerializeField] private CanvasRenderer healthImage;
    
    [SerializeField]
    private Color regularHealthColor = Color.green;
    [SerializeField]
    private Color lowHealthColor = Color.red;

    private void Start()
    {
        if(healthImage != null)
            healthImage.SetColor(regularHealthColor);
        
        ResourceManager resourceManager = ((PlayerDependency) PlayerDependency).GetComponent<ResourceManager>();
        CutBoost cutBoost = ((PlayerDependency) PlayerDependency).GetComponent<CutBoost>();
        
        scoreText.text = ScoreManager.Score.ToString(CultureInfo.InvariantCulture);
        healthSlider.value = 1;
        
        if(ScoreManager != null)
            ScoreManager.OnScoreUpdated += f =>
            {
                if(scoreText != null)
                    scoreText.text = f.ToString(CultureInfo.InvariantCulture);
            };
        
        if(resourceManager != null)
            resourceManager.onDamaged += f =>
            {
                if(healthSlider != null)
                    healthSlider.value = (float)resourceManager.Health/resourceManager.spaceshipStats.maxHealth;
            };

        if(cutBoost != null)
            cutBoost.onCutBoost += () =>
            {
                if(healthImage != null)
                    healthImage.SetColor(lowHealthColor);
            };
    }
}
