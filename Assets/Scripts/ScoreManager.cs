﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using Sirenix.Serialization;
using UnityEngine;

public class ScoreManager : SerializedMonoBehaviour, IScoreManager
{
    private int _score;

    public Action<float> OnScoreUpdated { get; set; }
    public int Score
    {
        get => _score;
        set
        {
            _score = value;
            OnScoreUpdated?.Invoke(_score);
        }
    }

}
