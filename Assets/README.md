# README: GAD-SCHMUP HERDLICKA

## Controls

 - `WS` => Thrust
 - `Move Mouse` => Rotate
 - `AD` => Roll
 - `LShift` while moving forward => Boost
 - `M1` => Shoot

## Goal

Dodge bullets from enemies.

Gain points for damaging/destroying enemy spaceships.

## Feedback Loops

### positive loop (for enemies)
If you have less than 40% health you cannot boost anymore
### negative loop
The more enemies you kill the more are spawned